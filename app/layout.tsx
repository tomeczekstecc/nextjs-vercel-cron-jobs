import './globals.css'
import {Analytics} from '@vercel/analytics/react' ;

export const metadata = {
    title: 'Create Next App',
    description: 'Generated by create next app',
}

export default function RootLayout({
                                       children,
                                   }: {
    children: React.ReactNode
}) {
    return (
        <html lang="en">

        <Analytics/>
        <body>{children}</body>
        </html>
    )
}
