import mailgun from 'mailgun-js'


const weatherKey = process.env.WEATHER_API_KEY
const mailGunApiKey = process.env.MAILGUN_PRIVATE_API_KEY
const mailGunDomain = process.env.MAILGUN_DOMAIN

export async function GET(request: Request) {
    const mg = mailgun({apiKey: mailGunApiKey!, domain: mailGunDomain!});

    const data = {
        from: 'Tomasz Steć <mailgun@tomeczekstecc-gmail.com>',
        // to: 'boi@tgory.sr.gov.pl',
        to: 'tomeczekstecc@gmail.com',
        subject: 'Przypomnienie o sprawie I C 1327/21',
        html: `
        <h1> Przypomnienie o sprawie I C 1327/21 </h1>
           <a href="https://portal.katowice.sa.gov.pl/#/sprawy/15382507/szczegoly" >https://portal.katowice.sa.gov.pl/#/sprawy/15382507/szczegoly</a>
`
    };

    await mg.messages().send(data, function (error, body) {
        if (error) console.log(error);
        console.log(body?.message, 'body');
    });

    return new Response('Hello, Vercel Cron!', {
        status: 200,
    })
}

